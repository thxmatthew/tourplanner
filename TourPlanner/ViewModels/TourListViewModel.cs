﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.EntityFrameworkCore;
using TourPlanner.Models;
using TourPlanner.Services;
using TourPlanner.Views;
using Xceed.Wpf.Toolkit.Core.Converters;

namespace TourPlanner.ViewModels
{
    public partial class TourListViewModel : ObservableRecipient
    {
        [ObservableProperty]
        public static ObservableCollection<Tour> _allTours;

        [ObservableProperty]
        public static Tour _selectedTour;

        public TourListViewModel()
        {
            AllTours = new(TourHandler.GetAllTours());
            SelectedTour = AllTours.First();
        }

        [RelayCommand]
        public static void OpenAddTourWindow()
        {
            var addTourWindow = new AddTourWindow();
            addTourWindow.ShowDialog();
        }

        [RelayCommand]
        public void RemoveTour()
        {
            TourHandler.DeleteTour(SelectedTour);
            SelectedTour = AllTours.First();
        }

        [RelayCommand]
        public void OpenEditTourWindow()
        {
            var editTourWindow = new EditTourWindow();
            var result = editTourWindow.ShowDialog();
            SelectedTour = AllTours.Last();
        }

    }
}
