﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using TourPlanner.Models;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    public partial class EditTourWindowViewModel : ObservableRecipient
    {
        private Tour currTour;

        [ObservableProperty]
        private string _nameInput;

        [ObservableProperty]
        private string _descriptionInput;

        [ObservableProperty]
        private string _fromInput;

        [ObservableProperty]
        private string _toInput;

        [ObservableProperty]
        private string _transportTypeInput;

        public EditTourWindowViewModel()
        {
            currTour = TourListViewModel._selectedTour;
            _nameInput = currTour.Name;
            _descriptionInput = currTour.Description;
            _fromInput = currTour.From;
            _toInput = currTour.To;
            _transportTypeInput = currTour.TransportType;
        }

        [RelayCommand]
        void EditTour(Window window)
        {
            currTour.Name = NameInput;
            currTour.Description = DescriptionInput;
            currTour.From = FromInput;
            currTour.To = ToInput;
            currTour.TransportType = _transportTypeInput;
            TourHandler.UpdateTour(currTour);
            if (window != null) window.Close();
        }

    }
}
