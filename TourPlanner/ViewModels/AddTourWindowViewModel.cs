﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MahApps.Metro.Actions;
using TourPlanner.Models;
using TourPlanner.Services;

namespace TourPlanner.ViewModels
{
    public partial class AddTourWindowViewModel : ObservableRecipient
    {
        [ObservableProperty]
        private string _nameInput;

        [ObservableProperty]
        private string _descriptionInput;

        [ObservableProperty]
        private string _fromInput;

        [ObservableProperty]
        private string _toInput;

        [ObservableProperty]
        private string _transportTypeInput;

        [RelayCommand]
        void CreateNewTour(Window window)
        {
            var newTour = new Tour()
            {
                Name = _nameInput,
                Description = _descriptionInput,
                From = _fromInput,
                To = _toInput,
                TransportType = _transportTypeInput,
                EstimatedTime = "Time_Test",
                RouteInformation = "routeInfo_Test",
                TourDistance = "distance_test"
            };
            TourHandler.CreateTour(newTour);
            if (window != null) window.Close();
        }

    }
}
