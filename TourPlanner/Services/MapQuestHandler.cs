﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.JavaScript;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using TourPlanner.Models;

namespace TourPlanner.Services
{
    public static class MapQuestHandler
    {
        private static string _baseUriDistance = "https://www.mapquestapi.com/directions/v2/route";
        private static string _baseUriStaticMap = "https://www.mapquestapi.com/staticmap/v5/map";

        private static string _mapQuestApiKey = "AjwxawuDbieLMdy0Qt1MErJgC0wWlpAX";

        private static HttpClient _distanceClient = new();

        public static async Task<MapQuestInfos> GetMapQuestInfos(string from, string to, string transportType)
        {
            try
            {
                var builder = new UriBuilder(_baseUriDistance);
                builder.Port = -1;
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["key"] = _mapQuestApiKey;
                query["from"] = from;
                query["to"] = to;
                query["unit"] = "k";
                query["routeType"] = transportType;
                builder.Query = query.ToString() ?? string.Empty;
                string url = builder.ToString();

                string responseBody = await _distanceClient.GetStringAsync(url);
                var content = JObject.Parse(responseBody);

                var info = content.SelectToken("info"); 
                var status = info?.Value<int>("statuscode");

                // TODO: add exception class
                if (status != 0) throw new Exception("Route not found!");

                var route = content.SelectToken("route");
                var distance = route.Value<double>("distance");
                var time = route.Value<string>("formattedTime");
                var routeMapPath = await GetRouteMap();

                return new MapQuestInfos(distance, time, routeMapPath);
            }
            catch (HttpRequestException e)
            {
                Debug.WriteLine("\nException Caught!");
                Debug.WriteLine("Message :{0} ", e.Message);
            }

            return null;
        }

        private static async Task<string> GetRouteMap()
        {
            var builder = new UriBuilder(_baseUriStaticMap);
            builder.Port = -1;
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["key"] = _mapQuestApiKey;
            query["start"] = "Vienna";
            query["end"] = "Berlin";
            query["format"] = "png";
            builder.Query = query.ToString() ?? string.Empty;
            string url = builder.ToString();

            var response = await _distanceClient.GetByteArrayAsync(url);
            return SaveRouteMap(response);
        }

        private static string SaveRouteMap(byte[] map)
        {
            string folderPath = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName).FullName;
            string folderName = Path.Combine(folderPath, "RouteMapImages");
            Directory.CreateDirectory(folderName);

            string filename = $"map_{DateTime.Now.ToString("dd'-'MM'-'yyyy'T'HH'_'mm'_'ss")}.png";
            string filePath = Path.Combine(folderName, filename);
            var file = File.Create(filePath);
            file.Write(map);
            file.Close();

            return filePath;
        }
    }
}
