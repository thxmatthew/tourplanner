﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TourPlanner.Models;
using TourPlanner.ViewModels;

namespace TourPlanner.Services
{
    public static class TourHandler
    {
        public static ObservableCollection<Tour> GetAllTours()
        {
            var tpc = new TourPlannerContext();
            return new ObservableCollection<Tour>(tpc.Tours);
        }

        // creates a tour
        public static void CreateTour(Tour tour)
        {
            var tpc = new TourPlannerContext();
            tpc.Tours.Add(tour);
            tpc.SaveChanges();
            TourListViewModel._allTours.Add(tour);
        }

        // creates multiple tours
        public static void CreateTour(List<Tour> tours)
        {
            using var tpc = new TourPlannerContext();
            tpc.Tours.AddRange(tours);
            tpc.SaveChanges();
        }

        // updates a tour
        public static void UpdateTour(Tour tour)
        {
            using var tpc = new TourPlannerContext();
            tpc.Tours.Update(tour);
            tpc.SaveChanges();
            TourListViewModel._allTours.Remove(tour);
            TourListViewModel._allTours.Add(tour);
        }

        // updates multiple tours
        public static void UpdateTour(List<Tour> tours)
        {
            using var tpc = new TourPlannerContext();
            tpc.Tours.UpdateRange(tours);
            tpc.SaveChanges();
        }

        // deletes a tour
        public static void DeleteTour(Tour tour)
        {
            using var tpc = new TourPlannerContext();
            tpc.Tours.Remove(tour);
            tpc.SaveChanges();
            TourListViewModel._allTours.Remove(tour);
        }

        // deletes multiple tours
        public static void DeleteTour(List<Tour> tours)
        {
            using var tpc = new TourPlannerContext();
            tpc.Tours.RemoveRange(tours);
            tpc.SaveChanges();
        }
    }
}
