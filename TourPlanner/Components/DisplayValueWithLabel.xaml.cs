﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;

namespace TourPlanner.Components
{
    /// <summary>
    /// Interaktionslogik für DisplayValueWithLabel.xaml
    /// </summary>
    public partial class DisplayValueWithLabel : UserControl
    {
        public static readonly DependencyProperty ValueProperty = 
            DependencyProperty.Register(nameof(DisplayValueWithLabel.Value), 
                typeof(string), 
                typeof(DisplayValueWithLabel), 
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public DisplayValueWithLabel()
        {
            InitializeComponent();
            LayoutGrid.DataContext = this;
        }

        public String Label { get; set; }

        public String Value
        {
            get => (string)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }
    }
}
