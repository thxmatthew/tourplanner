﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TourPlanner.Components
{
    /// <summary>
    /// Interaktionslogik für CustomTextInput.xaml
    /// </summary>
    public partial class CustomTextInput : UserControl
    {
        public CustomTextInput()
        {
            InitializeComponent();
            LayoutGrid.DataContext = this;
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(CustomTextInput.Value),
                typeof(string),
                typeof(CustomTextInput),
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public String Label { get; set; }

        public String Value
        {
            get => (string)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public String Watermark { get; set; }
    }
}
