﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TourPlanner.Models.Enums
{
    public enum TransportType
    {
        Fastest,
        Shortest,
        Pedestrian,
        Bicycle
    }
}
