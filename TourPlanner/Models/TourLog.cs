﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TourPlanner.Models
{
    [Table("tour_logs")]
    public class TourLog : ObservableObject
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("done_at")]
        [Required]
        public DateTime DoneAt { get; set; }

        [Column("comment")]
        public string Comment { get; set; }

        [Column("difficulty")]
        [Required]
        public int Difficulty { get; set; }

        [Column("total_time")]
        [Required]
        public int TotalTime { get; set; }

        [Column("rating")]
        [Required]
        public float Rating { get; set; }

        public Tour Tour { get; set; }
    }
}
