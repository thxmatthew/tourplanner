﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TourPlanner.Models
{
    [Table("tours")]
    public class Tour : ObservableObject
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("name")]
        [Required]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("from")]
        [Required]
        public string From { get; set; }

        [Column("to")]
        [Required]
        public string To { get; set; }

        [Column("transport_type")]
        public string TransportType { get; set; }

        [Column("tour_distance")]
        public string TourDistance { get; set; }

        [Column("estimated_time")]
        public string EstimatedTime { get; set; }

        // image with the tour map
        [Column("route_information")]
        public string RouteInformation { get; set; }

        [ForeignKey("tour_id")]
        public ICollection<TourLog> TourLogs { get; set; }
    }
}
