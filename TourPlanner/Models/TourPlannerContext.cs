﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System.Net;
using System.Xml.Linq;

namespace TourPlanner.Models
{
    public class TourPlannerContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // TODO: Change to according values
            modelBuilder.Entity<Tour>().HasData(
            new Tour { Id = 1, Name = "Wienerwald", Description = "Viele Bäume hier", From = "Wien", To = "Wald", EstimatedTime = "1", TransportType = "Transport Type 1", TourDistance = "1km", RouteInformation = "Bild von Bäumen" },
            new Tour { Id = 2, Name = "Dopplerhütte", Description = "Hier wohnt Doppler", From = "Wiener Neustadt", To = "Felixdorf", EstimatedTime = "5", TransportType = "Transport Type 2", TourDistance = "2km", RouteInformation = "Bild von Dopplers Hütte" },
            new Tour { Id = 3, Name = "Figlwarte", Description = "Hier wartet man auf Figl", From = "Figl", To = "Warte", EstimatedTime = "2", TransportType = "Transport Type 3", TourDistance = "3km", RouteInformation = "Bild von Figl" },
            new Tour { Id = 4, Name = "Dorfrunde", Description = "Eine Runde ums Dorf", From = "Dorfbeginn", To = "Dorfende", EstimatedTime = "4", TransportType = "Transport Type 4", TourDistance = "4km", RouteInformation = "Bild vom Dorf" }
            );
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = ConfigurationManager.ConnectionStrings["TourPlannerDbConfig"].ToString();
            optionsBuilder.UseNpgsql(connString);

        }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<TourLog> TourLogs { get; set; }
    }
}
