﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace TourPlanner.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tours",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: false),
                    description = table.Column<string>(type: "text", nullable: true),
                    from = table.Column<string>(type: "text", nullable: false),
                    to = table.Column<string>(type: "text", nullable: false),
                    transport_type = table.Column<string>(type: "text", nullable: true),
                    tour_distance = table.Column<string>(type: "text", nullable: true),
                    estimated_time = table.Column<string>(type: "text", nullable: true),
                    route_information = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tours", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "tour_logs",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    done_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    comment = table.Column<string>(type: "text", nullable: true),
                    difficulty = table.Column<int>(type: "integer", nullable: false),
                    total_time = table.Column<int>(type: "integer", nullable: false),
                    rating = table.Column<float>(type: "real", nullable: false),
                    tour_id = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tour_logs", x => x.id);
                    table.ForeignKey(
                        name: "FK_tour_logs_tours_tour_id",
                        column: x => x.tour_id,
                        principalTable: "tours",
                        principalColumn: "id");
                });

            migrationBuilder.InsertData(
                table: "tours",
                columns: new[] { "id", "description", "estimated_time", "from", "name", "route_information", "to", "tour_distance", "transport_type" },
                values: new object[,]
                {
                    { 1, "Viele Bäume hier", "1", "Wien", "Wienerwald", "Bild von Bäumen", "Wald", "1km", "Transport Type 1" },
                    { 2, "Hier wohnt Doppler", "5", "Wiener Neustadt", "Dopplerhütte", "Bild von Dopplers Hütte", "Felixdorf", "2km", "Transport Type 2" },
                    { 3, "Hier wartet man auf Figl", "2", "Figl", "Figlwarte", "Bild von Figl", "Warte", "3km", "Transport Type 3" },
                    { 4, "Eine Runde ums Dorf", "4", "Dorfbeginn", "Dorfrunde", "Bild vom Dorf", "Dorfende", "4km", "Transport Type 4" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_tour_logs_tour_id",
                table: "tour_logs",
                column: "tour_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tour_logs");

            migrationBuilder.DropTable(
                name: "tours");
        }
    }
}
