﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using TourPlanner.Models;

#nullable disable

namespace TourPlanner.Migrations
{
    [DbContext(typeof(TourPlannerContext))]
    [Migration("20230619190356_InitialCreate")]
    partial class InitialCreate
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("TourPlanner.Models.Tour", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<string>("Description")
                        .HasColumnType("text")
                        .HasColumnName("description");

                    b.Property<string>("EstimatedTime")
                        .HasColumnType("text")
                        .HasColumnName("estimated_time");

                    b.Property<string>("From")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("from");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("name");

                    b.Property<string>("RouteInformation")
                        .HasColumnType("text")
                        .HasColumnName("route_information");

                    b.Property<string>("To")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("to");

                    b.Property<string>("TourDistance")
                        .HasColumnType("text")
                        .HasColumnName("tour_distance");

                    b.Property<string>("TransportType")
                        .HasColumnType("text")
                        .HasColumnName("transport_type");

                    b.HasKey("Id");

                    b.ToTable("tours");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Viele Bäume hier",
                            EstimatedTime = "1",
                            From = "Wien",
                            Name = "Wienerwald",
                            RouteInformation = "Bild von Bäumen",
                            To = "Wald",
                            TourDistance = "1km",
                            TransportType = "Transport Type 1"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Hier wohnt Doppler",
                            EstimatedTime = "5",
                            From = "Wiener Neustadt",
                            Name = "Dopplerhütte",
                            RouteInformation = "Bild von Dopplers Hütte",
                            To = "Felixdorf",
                            TourDistance = "2km",
                            TransportType = "Transport Type 2"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Hier wartet man auf Figl",
                            EstimatedTime = "2",
                            From = "Figl",
                            Name = "Figlwarte",
                            RouteInformation = "Bild von Figl",
                            To = "Warte",
                            TourDistance = "3km",
                            TransportType = "Transport Type 3"
                        },
                        new
                        {
                            Id = 4,
                            Description = "Eine Runde ums Dorf",
                            EstimatedTime = "4",
                            From = "Dorfbeginn",
                            Name = "Dorfrunde",
                            RouteInformation = "Bild vom Dorf",
                            To = "Dorfende",
                            TourDistance = "4km",
                            TransportType = "Transport Type 4"
                        });
                });

            modelBuilder.Entity("TourPlanner.Models.TourLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<string>("Comment")
                        .HasColumnType("text")
                        .HasColumnName("comment");

                    b.Property<int>("Difficulty")
                        .HasColumnType("integer")
                        .HasColumnName("difficulty");

                    b.Property<DateTime>("DoneAt")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("done_at");

                    b.Property<float>("Rating")
                        .HasColumnType("real")
                        .HasColumnName("rating");

                    b.Property<int>("TotalTime")
                        .HasColumnType("integer")
                        .HasColumnName("total_time");

                    b.Property<int?>("tour_id")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("tour_id");

                    b.ToTable("tour_logs");
                });

            modelBuilder.Entity("TourPlanner.Models.TourLog", b =>
                {
                    b.HasOne("TourPlanner.Models.Tour", "Tour")
                        .WithMany("TourLogs")
                        .HasForeignKey("tour_id");

                    b.Navigation("Tour");
                });

            modelBuilder.Entity("TourPlanner.Models.Tour", b =>
                {
                    b.Navigation("TourLogs");
                });
#pragma warning restore 612, 618
        }
    }
}
